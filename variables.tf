variable "subnets" {
    description = "Number of subnets to create"
}

variable "name" {
    type = list
    description = "Names for the Subnet"
}

variable "rg" {
    type = string
    description = "Name of the resource Group"
}

variable "vnet" {
    type = string
    description = "Network for the subnets"
}

variable "address_prefixes" {
    type = list
    description = "address for each subnet"
}