# Azure Subnets Module

- Creation of various subnets for Azure VNET 

# Inputs 

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| VPC | The ID of the VPC | String | - |:yes:|

# Outputs 

| Name | Description |
|------|-------------|
| Subnets Map | ID, Address and Name of every subnet created |

# Usage

  
