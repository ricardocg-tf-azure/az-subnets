output "subnets" {
    value = {
        "id"            = azurerm_subnet.myterraformsubnet.*.id
        "name"          = azurerm_subnet.myterraformsubnet.*.name
        "cidr_block"    = azurerm_subnet.myterraformsubnet.*.address_prefixes
    }

    description = "Map of the id and cidr for every subnet created"
}