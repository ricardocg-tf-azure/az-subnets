output "subnets" {
    value       = module.subnets.subnets
    description = "Map of the id and cidr for every subnet created"
}