
module "rg" {
    source              = "git::https://gitlab.com/ricardocg-tf-azure/az-resource-group.git?ref=master"
    name                = "some_name"
}

module "vnet" {
    source              = "git::https://gitlab.com/ricardocg-tf-azure/az-virtual-network.git?ref=master"
    name                = "vnet_test"
    address             = ["10.0.0.0/16"]
    location            = "eastus"
    rg_name             = module.rg.name
    env                 = "test"
}

module "subnets" {
    source                  = "../../../"
    subnets                 = "2"
    name                    = ["subnet1","subnet2"]
    rg                      = module.rg.name
    vnet                    = module.vnet.name
    address_prefixes        = ["10.0.3.0/26", "10.0.4.0/26"]
}